package uk.co.wowcher;

import java.util.HashMap;
import java.util.Map;

class Clock {

    private String getClockHour(int hour) {
        Map<Integer, String> clockHours = new HashMap<>();

        clockHours.put(0, "zero");
        clockHours.put(1, "one");
        clockHours.put(2, "two");
        clockHours.put(3, "three");
        clockHours.put(4, "four");
        clockHours.put(5, "five");
        clockHours.put(6, "six");
        clockHours.put(7, "seven");
        clockHours.put(8, "eight");
        clockHours.put(9, "nine");
        clockHours.put(10, "ten");
        clockHours.put(11, "eleven");
        clockHours.put(12, "twelve");
        clockHours.put(13, "thirteen");
        clockHours.put(14, "fourteen");
        clockHours.put(15, "fifteen");
        clockHours.put(16, "sixteen");
        clockHours.put(17, "seventeen");
        clockHours.put(18, "eighteen");
        clockHours.put(19, "nineteen");
        clockHours.put(20, "twenty");
        clockHours.put(21, "twenty one");
        clockHours.put(22, "twenty two");
        clockHours.put(23, "twenty three");

        return clockHours.get(hour);
    }


    private String getClockMinute(int minute) {
        Map<Integer, String> clockMinutes = new HashMap<>();
        clockMinutes.put(0, "zero");
        clockMinutes.put(1, "one");
        clockMinutes.put(2, "two");
        clockMinutes.put(3, "three");
        clockMinutes.put(4, "four");
        clockMinutes.put(5, "five");
        clockMinutes.put(6, "six");
        clockMinutes.put(7, "seven");
        clockMinutes.put(8, "eight");
        clockMinutes.put(9, "nine");
        clockMinutes.put(10, "ten");
        clockMinutes.put(11, "eleven");
        clockMinutes.put(12, "twelve");
        clockMinutes.put(13, "thirteen");
        clockMinutes.put(14, "fourteen");
        clockMinutes.put(15, "fifteen");
        clockMinutes.put(16, "sixteen");
        clockMinutes.put(17, "seventeen");
        clockMinutes.put(18, "eighteen");
        clockMinutes.put(19, "nineteen");
        clockMinutes.put(20, "twenty");
        clockMinutes.put(21, "twenty one");
        clockMinutes.put(22, "twenty two");
        clockMinutes.put(23, "twenty three");
        clockMinutes.put(24, "twenty four");
        clockMinutes.put(25, "twenty five");
        clockMinutes.put(26, "twenty six");
        clockMinutes.put(27, "twenty seven");
        clockMinutes.put(28, "twenty eight");
        clockMinutes.put(29, "twenty nine");
        clockMinutes.put(30, "thirty");
        clockMinutes.put(31, "thirty one");
        clockMinutes.put(32, "thirty two");
        clockMinutes.put(33, "thirty three");
        clockMinutes.put(34, "thirty four");
        clockMinutes.put(35, "thirty five");
        clockMinutes.put(36, "thirty six");
        clockMinutes.put(37, "thirty seven");
        clockMinutes.put(38, "thirty eight");
        clockMinutes.put(39, "thirty nine");
        clockMinutes.put(40, "forty");
        clockMinutes.put(41, "forty one");
        clockMinutes.put(42, "forty two");
        clockMinutes.put(43, "forty three");
        clockMinutes.put(44, "forty four");
        clockMinutes.put(45, "forty five");
        clockMinutes.put(46, "forty six");
        clockMinutes.put(47, "forty seven");
        clockMinutes.put(48, "forty eight");
        clockMinutes.put(49, "forty nine");
        clockMinutes.put(50, "fifty");
        clockMinutes.put(51, "fifty one");
        clockMinutes.put(52, "fifty two");
        clockMinutes.put(53, "fifty three");
        clockMinutes.put(54, "fifty four");
        clockMinutes.put(55, "fifty five");
        clockMinutes.put(56, "fifty six");
        clockMinutes.put(57, "fifty seven");
        clockMinutes.put(58, "fifty eight");
        clockMinutes.put(59, "fifty nine");

        return clockMinutes.get(minute);
    }

    String getTime(int hour, int minute) {

        if (hour == 12 && minute == 0) {
            return "It's Midday";
        } else if (hour == 0 && minute == 0){
            return "It's Midnight";
        } return "It's " + getClockHour(hour) + " " + getClockMinute(minute);
    }
}
