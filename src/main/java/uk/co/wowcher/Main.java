package uk.co.wowcher;

import java.time.LocalTime;

public class Main {
    public static void main(String[] args) {
        LocalTime localTime = LocalTime.now();

        int hour = localTime.getHour();
        int minute = localTime.getMinute();

        Clock clock = new Clock();
        System.out.println(clock.getTime(hour, minute));

    }
}
