package uk.co.wowcher;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ClockTest {

    private Clock clock = new Clock();

    @Test
    public void testCorrectOutputForMidday() {
        String correct_output = "It's Midday";
        int hour = 12;
        int minute = 0;
        String output = clock.getTime(hour, minute);
        assertEquals(correct_output, output);

    }

    @Test
    public void testCorrectOutputForMidnight() {
        String correct_output = "It's Midnight";
        int hour = 0;
        int minute = 0;
        String output = clock.getTime(hour, minute);
        assertEquals(correct_output, output);
    }

    @Test
    public void testCorrectHourStringOutput() {
        int testHour = 8;
        String correct_output = "eight";
        String output = clock.getClockHour(testHour);
        assertEquals(correct_output, output);
    }

    @Test
    public void testCorrectMinuteStringOutput() {
        int testMinute = 34;
        String correct_output = "thirty four";
        String output = clock.getClockMinute(testMinute);
        assertEquals(correct_output, output);
    }

    @Test
    public void testTalkingClockOutput() {
        String correct_output = "It's eight thirty four";
        int testHour = 8;
        int testMinute = 34;
        String output = clock.getTime(testHour, testMinute);
        assertEquals(correct_output, output);

    }
}
